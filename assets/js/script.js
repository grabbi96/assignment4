// string method

let exampleOne = "hello world";
document.write(exampleOne.charAt(4)+"</br>");



let exampleTwo = "hello world";
document.write(exampleTwo.charCodeAt("w")+"</br>");



let exampleThreeFirst = "hello";
let exampleThreeSecend = " world";
let exampleThree = exampleThreeFirst.concat(exampleThreeSecend);
document.write(exampleThree+"</br>");



let exampleFour = "helloworld";
document.write(exampleFour.endsWith("d")+"</br>");


let exampleFive = "helloworld";
document.write(exampleFive.includes("l")+"</br>");



let exampleSix = "hello  world";
document.write(exampleSix.indexOf("w")+"</br>");


let exampleSeven = "hello  world";
document.write(exampleSeven.lastIndexOf("world")+"</br>");


let exampleEight = "hello  world";
document.write(exampleEight.match("wer")+"</br>");



let exampleNine = "hello  world ";
document.write(exampleNine.repeat(4)+"</br>");


let exampleTenFirst = "hello world";
let exampleTenSecend = "hello pondit";

let exampleTen = exampleTenFirst.replace(exampleTenFirst, exampleTenSecend);
document.write(exampleTen+"</br>");


let exampleEleven = "hello  world ";
document.write(exampleEleven.search("r")+"</br>");


let exampleTwelve = "hello  world ";
document.write(exampleTwelve.slice(2, 8)+"</br>");


let exampleThirteen = "hello  world ";
document.write(exampleThirteen.substr(2)+"</br>");




let exampleFourteen = "helloworld";
document.write(exampleFourteen.startsWith("h")+"</br>");


let exampleFifteen = "helloworld";
document.write(exampleFifteen.split("h")+"</br>"); 


let exampleSixteen = "hello  world ";
document.write(exampleSixteen.substring(2, 8)+"</br>");



let exampleSeventeen = "hello  world ";
document.write(exampleSeventeen.toLowerCase()+"</br>");



let exampleEighteen = "hello  world ";
document.write(exampleEightteen.toUpperCase()+"</br>");



let exampleNineteen = "   hello  world      ";
document.write(exampleNineteen.trim()+"</br>");



let exampletwenty= "hello  world";
document.write(exampletwenty.valueOf()+"</br>");

